from rest_framework import viewsets
from inventory.serializers.product import ProductSerializer
from inventory.models import Product

class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
