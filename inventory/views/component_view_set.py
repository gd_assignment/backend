from rest_framework import viewsets
from inventory.serializers.component import ComponentSerializer
from inventory.models import Component

class ComponentViewSet(viewsets.ModelViewSet):
    serializer_class = ComponentSerializer
    queryset = Component.objects.all()
