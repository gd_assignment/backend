from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from inventory.serializers.available_product import AvailableProductSerializer
from inventory.models import Product, Article, Component

class AvailableProductViewSet(viewsets.ModelViewSet):
    serializer_class = AvailableProductSerializer

    def get_queryset(self):
        qs = Product.objects.all()

        # todo: optimize number of queries

        return qs

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        serializer = self.get_serializer(queryset, many=True)
        serialized_data: list = serializer.data

        def is_sufficient_stock(product):
            for component in product['components']:
                return component["amount"] <= component['article']['stock']

        def extend_with_available_amount(product):
            max_amount = 0

            for component in product['components']:
                max_amount = max(max_amount, component['article']['stock'] // component["amount"])

            product["max_amount"] = max_amount

            return product

        available_products = [extend_with_available_amount(product) for product in serialized_data if is_sufficient_stock(product)]

        return Response(available_products)

    @action(detail=True, methods=["PATCH"])
    def sell(self, request, pk=None):

        try:
            product = Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            return Response({'error': 'Product not found'}, status=status.HTTP_404_NOT_FOUND)

        for component in product.components.all():
            article: Article = component.article
            
            if article.stock >= component.amount:
                article.stock -= component.amount
                article.save()

        return Response({'status': 'OK'})
