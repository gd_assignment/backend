from django.contrib import admin
from inventory.models import Product, Article, Component
# Register your models here.

admin.site.register(Product)
admin.site.register(Article)
admin.site.register(Component)
