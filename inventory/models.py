from djmoney.models.fields import MoneyField
from django.db import models

# Create your models here.

class Product(models.Model):
    name = models.CharField(null=False, max_length=255)
    price = MoneyField(max_digits=10, decimal_places=4, default_currency='DKK')

    def __str__(self) -> str:
        return f"{self.name} ({self.price})"


class Article(models.Model):
    name = models.CharField(null=False, max_length=255)
    identification_number = models.CharField(null=False, max_length=64)
    stock = models.IntegerField(null=False, default=0)

    def __str__(self) -> str:
        return f"{self.name} ({self.identification_number})"


class Component(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="components")
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name="components")
    amount = models.IntegerField(null=False, default=0)

    def __str__(self) -> str:
        return f"{self.product.name} - {self.article.name} ({self.amount})"
    
