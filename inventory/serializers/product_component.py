from rest_framework import serializers

from inventory.models import Component
from inventory.serializers.article import ArticleSerializer

class ProductComponentSerializer(serializers.ModelSerializer):
    article = ArticleSerializer()

    class Meta:
        model = Component
        fields = [
            'amount',
            'article',
            'product',
        ]
