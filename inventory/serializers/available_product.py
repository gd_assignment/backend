from inventory.models import Product
from inventory.serializers.product_component import ProductComponentSerializer

from rest_framework import serializers

class AvailableProductSerializer(serializers.ModelSerializer):
    components = ProductComponentSerializer(many=True)

    class Meta:
        model = Product
        fields = [
            'id',
            'name',
            'price',
            'price_currency',
            'components'
        ]
