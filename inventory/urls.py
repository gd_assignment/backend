from django.urls import path, include
from rest_framework import routers

from inventory.views import available_product_view_set
from inventory.views import product_view_set
from inventory.views import article_view_set
from inventory.views import component_view_set

router = routers.DefaultRouter()
router.register(r'available_products', available_product_view_set.AvailableProductViewSet, basename="available_products")
router.register(r'products', product_view_set.ProductViewSet, basename="products")
router.register(r'articles', article_view_set.ArticleViewSet, basename="articles")
router.register(r'components', component_view_set.ComponentViewSet, basename="components")

urlpatterns = [
    path('', include(router.urls)),
]
