# syntax=docker/dockerfile:1
FROM python:3.9

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /code

RUN pip install --upgrade pip setuptools==57.5.0
COPY requirements.txt /code/
RUN pip install -r requirements.txt

COPY . /code/
