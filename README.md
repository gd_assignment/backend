# Backend

## External libs

- Django, for basic structure and ORM
- Django REST Framework to implement a REST interface
- Django CORS Headers, because the frontend is a Next.js app
- django-money to handle product prices in a failsafe way

## Install

1. Clone this repository
2. Copy .env.dist to .env
3. docker-compose up

## Upload data

### Via DRF API explorer

Go to http://localhost:8000 and upload products / articles / components in the forms below the corresponding API explorer page

### Via django admin

Open a shell in the container, and run

```
python manage.py createsuperuser
```

Then go to http://localhost:8000/admin and login with the provided credentials.

## Project structure

### Database

You can find the models in `inventory / models.py`

### API

The api is provided via ViewSets (similar to controllers in other MVC frameworks) in `invertory / views / *`
Routing is defined in `inventory / urls.py` and in `warehouse / urls.py`

### Serializers

JSON serializing is defined in `inventory / serializers / *`
